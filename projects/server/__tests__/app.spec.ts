import request from "supertest";
import { initApplication } from "../src/index";
describe("App", () => {
    let agent = request.agent(initApplication());
    beforeEach(() => {
        agent = request.agent(initApplication());
    });

    it("Sends back 'Hello World!'", async () => {
        await agent.get("/").expect(200, "Hello World!");
    });
});