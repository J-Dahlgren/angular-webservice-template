import http from "http";
import { initApplication } from "./app";
const port = process.env.SERVER_PORT || 8080;
const app = initApplication();

const server = http.createServer(app);
server.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});
