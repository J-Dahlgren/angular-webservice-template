module.exports = {    
    preset: "ts-jest",
    testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.ts$",
    globals: {
        "ts-jest": {
            tsConfig: "projects/shared/tsconfig.json",
            diagnostics: false
        }
    }
    
}