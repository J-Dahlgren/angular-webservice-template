# Angular Webservice Template

This project exist to reduce time spent setting up a typescript web project complete with frontend, server and shared project code.
It utilizes Lerna JS to manage dependencies and Jest to run tests.
By using Lerna, shared definitions and helper functions may be used in both the frontend and server but need only be written once. Any changes in the shared project will be "synced" to its dependants.

### Prerequisites

*  Node
*  NPM
```
npm install -g lerna typescript jest @angular/cli
```


### Install
```
npm install
lerna bootstrap
```
### Develop

In shared
```
tsc -w
```
In server
```
npm run auto
```
In frontend
```
ng serve -o
```

### Build
```
lerna run compile   // Compiles shared and server
ng build            // Builds frontend
```
### Test
```
jest // run from repo root dir
```

### License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Author

**Jesper Dahlgren**
